﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;

namespace CRM_Data_Extraction_Processor
{
    public class CRM_Accounts
    {
        public static OnDemandSession mSession;
        public static String username = "";
        public static String password = "";
        public static String servername = "";

        /*
         * Changes Made:
         * 1. 20170301 - Add Created By, Created Date, Modified By, Modified Date fields on the output dataset
         * 2. 20170331 - Add Group Member, Account Type, WR Terr, AC Terr Division fields on the queried fields
         * 3. 20170403 - Add OwnerId and OwnerFullName on the queried fields
         */
        public String getAccountDetails(OnDemandSession pSession, String pBookName, DateTime pCreatedDateFrom, DateTime pCreatedDateTo)
        {
            Int32 iAccountCtr = 0;
            bool hasValues = true;
            String result = "";

            try
            {
                while (hasValues == true)
                {
                    WSOD_Account_v2.Account svcAccount = new WSOD_Account_v2.Account();
                    WSOD_Account_v2.AccountQueryPage_Input inputAccountQuery = new WSOD_Account_v2.AccountQueryPage_Input();
                    WSOD_Account_v2.AccountQueryPage_Output outputAccountQuery = new WSOD_Account_v2.AccountQueryPage_Output();

                    inputAccountQuery.IncludeSubBooks = "true";

                    if (pBookName != "")
                    {
                    //    inputAccountQuery.ViewMode = "Context";
                        inputAccountQuery.BookName = pBookName;
                    }
                    else
                    {
                        inputAccountQuery.ViewMode = "Broadest";
                    }

                    inputAccountQuery.ListOfAccount = new WSOD_Account_v2.ListOfAccountQuery();
                    inputAccountQuery.ListOfAccount.pagesize = "100";
                    inputAccountQuery.ListOfAccount.startrownum = iAccountCtr.ToString();
                    inputAccountQuery.ListOfAccount.recordcountneeded = true;
                    
                    inputAccountQuery.ListOfAccount.Account = new WSOD_Account_v2.AccountQuery();
                    inputAccountQuery.ListOfAccount.Account.Id = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.AccountName = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.Location = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.Owner = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.CreatedDate = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.CreatedDateExt = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.CreatedByFullName = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.ModifiedBy = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.ModifiedDate = new WSOD_Account_v2.queryType();
                   
                    inputAccountQuery.ListOfAccount.Account.ListOfBook = new WSOD_Account_v2.ListOfBookQuery();
                    inputAccountQuery.ListOfAccount.Account.ListOfBook.Book = new WSOD_Account_v2.BookQuery();
                    inputAccountQuery.ListOfAccount.Account.ListOfBook.pagesize = "100";
                    inputAccountQuery.ListOfAccount.Account.ListOfBook.startrownum = "0";
                    inputAccountQuery.ListOfAccount.Account.ListOfBook.Book.BookName = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.ListOfBook.Book.BookId = new WSOD_Account_v2.queryType();

                    /*20170331-start of addtional requests*/
                    inputAccountQuery.ListOfAccount.Account.AccountType = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.plWR_Terr = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.plAC_Terr = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.DivisionName = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.DivisionId = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.IndexedPick0 = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.MainPhone = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.PrimaryBillToCountry = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.PrimaryShipToCountry = new WSOD_Account_v2.queryType();
                    /*20170331-end of addtional requests*/

                    /*20170403-start of additional request*/
                    inputAccountQuery.ListOfAccount.Account.OwnerId = new WSOD_Account_v2.queryType();
                    inputAccountQuery.ListOfAccount.Account.OwnerFullName = new WSOD_Account_v2.queryType();
                    /*20170403-end of additional request*/

                    inputAccountQuery.ListOfAccount.Account.searchspec = "([CreatedDate] >= '" + pCreatedDateFrom.ToShortDateString() + "' AND [CreatedDate] <= '" + pCreatedDateTo.ToShortDateString() + "')";
                    inputAccountQuery.ListOfAccount.Account.CreatedDate.sortorder = "ASC";

                    try
                    {
                        svcAccount.Url = pSession.GetURL();
                        svcAccount.CookieContainer = pSession.GetCookieContainer();
                        svcAccount.RequestEncoding = Encoding.UTF8;
                        svcAccount.Timeout = 1200000;

                        outputAccountQuery = svcAccount.AccountQueryPage(inputAccountQuery);

                        if (outputAccountQuery != null)
                        {
                            if (outputAccountQuery.ListOfAccount.Account != null)
                            {
                                if (outputAccountQuery.ListOfAccount.Account.Length >= 0)
                                {

                                    if (outputAccountQuery.ListOfAccount.Account.Length >= 100)
                                    {
                                        hasValues = true;
                                        iAccountCtr = iAccountCtr + 100;
                                    }
                                    else
                                    {
                                        hasValues = false;
                                        iAccountCtr = iAccountCtr + outputAccountQuery.ListOfAccount.Account.Length;
                                    }

                                    //insertcode to save the records on the csv file
                                    LogFile.Out(iAccountCtr + " account records has been extracted.");

                                    try
                                    {
                                        for (Int32 idxAcct = 0; idxAcct < outputAccountQuery.ListOfAccount.Account.Length; idxAcct++)
                                        {
                                            String pAccountRowId = outputAccountQuery.ListOfAccount.Account[idxAcct].Id;
                                            String pCreatedBy = outputAccountQuery.ListOfAccount.Account[idxAcct].CreatedByFullName;
                                            DateTime pCreatedDate = outputAccountQuery.ListOfAccount.Account[idxAcct].CreatedDate;
                                            String pModifiedBy = outputAccountQuery.ListOfAccount.Account[idxAcct].ModifiedBy.Substring(0, outputAccountQuery.ListOfAccount.Account[idxAcct].ModifiedBy.IndexOf(','));
                                            DateTime pModifiedDate = outputAccountQuery.ListOfAccount.Account[idxAcct].ModifiedDate;

                                            /*20170331-start of additional requests*/
                                            String pAccountType = outputAccountQuery.ListOfAccount.Account[idxAcct].AccountType;
                                            String pWRTerr = outputAccountQuery.ListOfAccount.Account[idxAcct].plWR_Terr;
                                            String pACTerr = outputAccountQuery.ListOfAccount.Account[idxAcct].plAC_Terr;
                                            String pDivisionName = outputAccountQuery.ListOfAccount.Account[idxAcct].DivisionName;
                                            String pDivisionId = outputAccountQuery.ListOfAccount.Account[idxAcct].DivisionId;
                                            String pGroupMember = outputAccountQuery.ListOfAccount.Account[idxAcct].IndexedPick0;
                                            String pMainPhone = outputAccountQuery.ListOfAccount.Account[idxAcct].MainPhone;
                                            String pBillToCountry = outputAccountQuery.ListOfAccount.Account[idxAcct].PrimaryBillToCountry;
                                            String pShipToCountry = outputAccountQuery.ListOfAccount.Account[idxAcct].PrimaryShipToCountry;
                                            /*20170331-end of additional requests*/

                                            /*20170401-start of additional requests*/
                                            String pOwnerId = outputAccountQuery.ListOfAccount.Account[idxAcct].OwnerId;
                                            String pOwnerFullName = outputAccountQuery.ListOfAccount.Account[idxAcct].OwnerFullName;
                                            /*20170401-end of additional requests*/

                                            if (outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book != null)
                                            {
                                                if (outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book.Length > 0)
                                                {
                                                    LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ". Has " + outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book.Length + " associated books.");

                                                    for (Int32 idxAcctBk = 0; idxAcctBk < outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book.Length; idxAcctBk++)
                                                    {
                                                        String pListOfBookName = outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book[idxAcctBk].BookName;

                                                        LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ": " + pListOfBookName);
                                                        result = LogFile.saveExtractedAccountDetails("Account", pAccountRowId, pListOfBookName, pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountType, pWRTerr, pACTerr, pDivisionId, pDivisionName,pGroupMember,pMainPhone,pBillToCountry,pShipToCountry,pOwnerId,pOwnerFullName);
                                                    }
                                                }
                                                else
                                                {
                                                    LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ". Has No Book Association.");
                                                    result = LogFile.saveExtractedAccountDetails("Account", pAccountRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountType, pWRTerr, pACTerr, pDivisionId, pDivisionName, pGroupMember, pMainPhone, pBillToCountry, pShipToCountry,pOwnerId, pOwnerFullName);
                                                }
                                            }
                                            else
                                            {
                                                LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ". Has No Book Association.");
                                                result = LogFile.saveExtractedAccountDetails("Account", pAccountRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountType, pWRTerr, pACTerr, pDivisionId, pDivisionName, pGroupMember, pMainPhone, pBillToCountry, pShipToCountry,pOwnerId,pOwnerFullName);
                                            }
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        LogFile.Out("Error on Extracting Account Details: " + ex.Message);
                                        result = "Error";
                                    }
                                }
                                else
                                {
                                    hasValues = false;
                                    result = "";
                                }
                            }
                            else
                            {
                                hasValues = false;
                                result = "";
                            }
                        }
                        else
                        {
                            hasValues = false;
                            result = "";
                        }
                    }
                    catch (Exception ex)
                    {
                        LogFile.Out("Error encountered on getAccountDetails(): " + ex.Message);
                        LogFile.Out("Retry accessing to CRM after error - Retry 1");

                        try
                        {
                            pSession.Destroy();

                            pSession = EnterLoginCredentials();

                            svcAccount.Url = pSession.GetURL();
                            svcAccount.CookieContainer = pSession.GetCookieContainer();
                            svcAccount.RequestEncoding = Encoding.UTF8;
                            svcAccount.Timeout = 1200000;

                            outputAccountQuery = svcAccount.AccountQueryPage(inputAccountQuery);

                            if (outputAccountQuery != null)
                            {
                                if (outputAccountQuery.ListOfAccount.Account != null)
                                {
                                    if (outputAccountQuery.ListOfAccount.Account.Length >= 0)
                                    {

                                        if (outputAccountQuery.ListOfAccount.Account.Length >= 100)
                                        {
                                            hasValues = true;
                                            iAccountCtr = iAccountCtr + 100;
                                        }
                                        else
                                        {
                                            hasValues = false;
                                            iAccountCtr = iAccountCtr + outputAccountQuery.ListOfAccount.Account.Length;
                                        }

                                        //insertcode to save the records on the csv file
                                        LogFile.Out(iAccountCtr + " account records has been extracted.");

                                        try
                                        {
                                            for (Int32 idxAcct = 0; idxAcct < outputAccountQuery.ListOfAccount.Account.Length; idxAcct++)
                                            {
                                                String pAccountRowId = outputAccountQuery.ListOfAccount.Account[idxAcct].Id;
                                                String pCreatedBy = outputAccountQuery.ListOfAccount.Account[idxAcct].CreatedByFullName;
                                                DateTime pCreatedDate = outputAccountQuery.ListOfAccount.Account[idxAcct].CreatedDate;
                                                String pModifiedBy = outputAccountQuery.ListOfAccount.Account[idxAcct].ModifiedBy.Substring(0, outputAccountQuery.ListOfAccount.Account[idxAcct].ModifiedBy.IndexOf(','));
                                                DateTime pModifiedDate = outputAccountQuery.ListOfAccount.Account[idxAcct].ModifiedDate;

                                                /*20170331-start of additional requests*/
                                                String pAccountType = outputAccountQuery.ListOfAccount.Account[idxAcct].AccountType;
                                                String pWRTerr = outputAccountQuery.ListOfAccount.Account[idxAcct].plWR_Terr;
                                                String pACTerr = outputAccountQuery.ListOfAccount.Account[idxAcct].plAC_Terr;
                                                String pDivisionName = outputAccountQuery.ListOfAccount.Account[idxAcct].DivisionName;
                                                String pDivisionId = outputAccountQuery.ListOfAccount.Account[idxAcct].DivisionId;
                                                String pGroupMember = outputAccountQuery.ListOfAccount.Account[idxAcct].IndexedPick0;
                                                String pMainPhone = outputAccountQuery.ListOfAccount.Account[idxAcct].MainPhone;
                                                String pBillToCountry = outputAccountQuery.ListOfAccount.Account[idxAcct].PrimaryBillToCountry;
                                                String pShipToCountry = outputAccountQuery.ListOfAccount.Account[idxAcct].PrimaryShipToCountry;
                                                /*20170331-end of additional requests*/

                                                /*20170401-start of additional requests*/
                                                String pOwnerId = outputAccountQuery.ListOfAccount.Account[idxAcct].OwnerId;
                                                String pOwnerFullName = outputAccountQuery.ListOfAccount.Account[idxAcct].OwnerFullName;
                                                /*20170401-end of additional requests*/


                                                if (outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book != null)
                                                {
                                                    if (outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book.Length > 0)
                                                    {
                                                        LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ". Has " + outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book.Length + " associated books.");

                                                        for (Int32 idxAcctBk = 0; idxAcctBk < outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book.Length; idxAcctBk++)
                                                        {
                                                            String pListOfBookName = outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book[idxAcctBk].BookName;

                                                            LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ": " + pListOfBookName);
                                                            result = LogFile.saveExtractedAccountDetails("Account", pAccountRowId, pListOfBookName, pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate,pAccountType,pWRTerr,pACTerr,pDivisionName,pDivisionId,pGroupMember,pMainPhone,pBillToCountry,pShipToCountry,pOwnerId,pOwnerFullName);
                                                        }

                                                    }
                                                    else
                                                    {
                                                        LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ". Has No Book Association.");
                                                        result = LogFile.saveExtractedAccountDetails("Account", pAccountRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate,pAccountType,pWRTerr,pACTerr,pDivisionName,pDivisionId,pGroupMember,pMainPhone,pBillToCountry,pShipToCountry,pOwnerId,pOwnerFullName);
                                                    }
                                                }
                                                else
                                                {
                                                    LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ". Has No Book Association.");
                                                    result = LogFile.saveExtractedAccountDetails("Account", pAccountRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate,pAccountType,pWRTerr,pACTerr,pDivisionName,pDivisionId,pGroupMember,pMainPhone,pBillToCountry,pShipToCountry,pOwnerId,pOwnerFullName);
                                                }
                                            }

                                        }
                                        catch (Exception ex2)
                                        {
                                            LogFile.Out("Error on Extracting Account Details: " + ex2.Message);
                                            result = "Error";
                                        }
                                    }
                                    else
                                    {
                                        hasValues = false;
                                        result = "";
                                    }
                                }
                                else
                                {
                                    hasValues = false;
                                    result = "";
                                }
                            }
                            else
                            {
                                hasValues = false;
                                result = "";
                            }
                        }
                        catch (Exception exInnerCatch2)
                        {
                            LogFile.Out("Error encountered on getAccountDetails(): " + exInnerCatch2.Message);
                            LogFile.Out("Retry accessing to CRM after error - Retry 2");

                            pSession.Destroy();

                            LogFile.Out("Processing of query will rest for 90 seconds. Please wait...");
                            System.Threading.Thread.Sleep(90000);
                            LogFile.Out("Processing will now resume.");

                            pSession = EnterLoginCredentials();

                            try
                            {
                                svcAccount = new WSOD_Account_v2.Account();
                                inputAccountQuery = new WSOD_Account_v2.AccountQueryPage_Input();
                                outputAccountQuery = new WSOD_Account_v2.AccountQueryPage_Output();

                                while (hasValues == true)
                                {
                                    inputAccountQuery.IncludeSubBooks = "true";

                                    if (pBookName != "")
                                    {
                                        //    inputAccountQuery.ViewMode = "Context";
                                        inputAccountQuery.BookName = pBookName;
                                    }
                                    else
                                    {
                                        inputAccountQuery.ViewMode = "Broadest";
                                    }

                                    inputAccountQuery.ListOfAccount = new WSOD_Account_v2.ListOfAccountQuery();
                                    inputAccountQuery.ListOfAccount.pagesize = "100";
                                    inputAccountQuery.ListOfAccount.startrownum = iAccountCtr.ToString();
                                    inputAccountQuery.ListOfAccount.recordcountneeded = true;

                                    inputAccountQuery.ListOfAccount.Account = new WSOD_Account_v2.AccountQuery();
                                    inputAccountQuery.ListOfAccount.Account.Id = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.AccountName = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.Location = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.Owner = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.CreatedDate = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.CreatedDateExt = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.CreatedByFullName = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.ModifiedBy = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.ModifiedDate = new WSOD_Account_v2.queryType();

                                    inputAccountQuery.ListOfAccount.Account.ListOfBook = new WSOD_Account_v2.ListOfBookQuery();
                                    inputAccountQuery.ListOfAccount.Account.ListOfBook.Book = new WSOD_Account_v2.BookQuery();
                                    inputAccountQuery.ListOfAccount.Account.ListOfBook.pagesize = "100";
                                    inputAccountQuery.ListOfAccount.Account.ListOfBook.startrownum = "0";
                                    inputAccountQuery.ListOfAccount.Account.ListOfBook.Book.BookName = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.ListOfBook.Book.BookId = new WSOD_Account_v2.queryType();

                                    /*20170331-start of additional requests*/
                                    inputAccountQuery.ListOfAccount.Account.AccountType = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.plWR_Terr = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.plAC_Terr = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.DivisionName = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.DivisionId = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.IndexedPick0 = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.MainPhone = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.PrimaryBillToCountry = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.PrimaryShipToCountry = new WSOD_Account_v2.queryType();
                                    /*20170331-end of additional requests*/

                                    /*20170403-start of additional request*/
                                    inputAccountQuery.ListOfAccount.Account.OwnerId = new WSOD_Account_v2.queryType();
                                    inputAccountQuery.ListOfAccount.Account.OwnerFullName = new WSOD_Account_v2.queryType();
                                    /*20170403-end of additional request*/

                                    inputAccountQuery.ListOfAccount.Account.searchspec = "([CreatedDate] >= '" + pCreatedDateFrom.ToShortDateString() + "' AND [CreatedDate] <= '" + pCreatedDateTo.ToShortDateString() + "')";
                                    inputAccountQuery.ListOfAccount.Account.CreatedDate.sortorder = "ASC";

                                    try
                                    {
                                        svcAccount.Url = pSession.GetURL();
                                        svcAccount.CookieContainer = pSession.GetCookieContainer();
                                        svcAccount.RequestEncoding = Encoding.UTF8;
                                        svcAccount.Timeout = 1200000;

                                        outputAccountQuery = svcAccount.AccountQueryPage(inputAccountQuery);

                                        if (outputAccountQuery != null)
                                        {
                                            if (outputAccountQuery.ListOfAccount.Account != null)
                                            {
                                                if (outputAccountQuery.ListOfAccount.Account.Length >= 0)
                                                {

                                                    if (outputAccountQuery.ListOfAccount.Account.Length >= 100)
                                                    {
                                                        hasValues = true;
                                                        iAccountCtr = iAccountCtr + 100;
                                                    }
                                                    else
                                                    {
                                                        hasValues = false;
                                                        iAccountCtr = iAccountCtr + outputAccountQuery.ListOfAccount.Account.Length;
                                                    }

                                                    //insertcode to save the records on the csv file
                                                    LogFile.Out(iAccountCtr + " account records has been extracted.");

                                                    try
                                                    {
                                                        for (Int32 idxAcct = 0; idxAcct < outputAccountQuery.ListOfAccount.Account.Length; idxAcct++)
                                                        {
                                                            String pAccountRowId = outputAccountQuery.ListOfAccount.Account[idxAcct].Id;
                                                            String pCreatedBy = outputAccountQuery.ListOfAccount.Account[idxAcct].CreatedByFullName;
                                                            DateTime pCreatedDate = outputAccountQuery.ListOfAccount.Account[idxAcct].CreatedDate;
                                                            String pModifiedBy = outputAccountQuery.ListOfAccount.Account[idxAcct].ModifiedBy.Substring(0, outputAccountQuery.ListOfAccount.Account[idxAcct].ModifiedBy.IndexOf(','));
                                                            DateTime pModifiedDate = outputAccountQuery.ListOfAccount.Account[idxAcct].ModifiedDate;

                                                            /*20170331-start of additional requests*/
                                                            String pAccountType = outputAccountQuery.ListOfAccount.Account[idxAcct].AccountType;
                                                            String pWRTerr = outputAccountQuery.ListOfAccount.Account[idxAcct].plWR_Terr;
                                                            String pACTerr = outputAccountQuery.ListOfAccount.Account[idxAcct].plAC_Terr;
                                                            String pDivisionName = outputAccountQuery.ListOfAccount.Account[idxAcct].DivisionName;
                                                            String pDivisionId = outputAccountQuery.ListOfAccount.Account[idxAcct].DivisionId;
                                                            String pGroupMember = outputAccountQuery.ListOfAccount.Account[idxAcct].IndexedPick0;
                                                            String pMainPhone = outputAccountQuery.ListOfAccount.Account[idxAcct].MainPhone;
                                                            String pBillToCountry = outputAccountQuery.ListOfAccount.Account[idxAcct].PrimaryBillToCountry;
                                                            String pShipToCountry = outputAccountQuery.ListOfAccount.Account[idxAcct].PrimaryShipToCountry;
                                                            /*20170331-end of additional requests*/

                                                            /*20170401-start of additional requests*/
                                                            String pOwnerId = outputAccountQuery.ListOfAccount.Account[idxAcct].OwnerId;
                                                            String pOwnerFullName = outputAccountQuery.ListOfAccount.Account[idxAcct].OwnerFullName;
                                                            /*20170401-end of additional requests*/

                                                            if (outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book != null)
                                                            {
                                                                if (outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book.Length > 0)
                                                                {
                                                                    LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ". Has " + outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book.Length + " associated books.");

                                                                    for (Int32 idxAcctBk = 0; idxAcctBk < outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book.Length; idxAcctBk++)
                                                                    {
                                                                        String pListOfBookName = outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book[idxAcctBk].BookName;

                                                                        LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ": " + pListOfBookName);
                                                                        result = LogFile.saveExtractedAccountDetails("Account", pAccountRowId, pListOfBookName, pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountType, pWRTerr, pACTerr, pDivisionName, pDivisionId, pGroupMember, pMainPhone, pBillToCountry, pShipToCountry,pOwnerId,pOwnerFullName);
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ". Has No Book Association.");
                                                                    result = LogFile.saveExtractedAccountDetails("Account", pAccountRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountType, pWRTerr, pACTerr, pDivisionName, pDivisionId, pGroupMember, pMainPhone, pBillToCountry, pShipToCountry,pOwnerId,pOwnerFullName);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ". Has No Book Association.");
                                                                result = LogFile.saveExtractedAccountDetails("Account", pAccountRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountType, pWRTerr, pACTerr, pDivisionName, pDivisionId, pGroupMember, pMainPhone, pBillToCountry, pShipToCountry,pOwnerId,pOwnerFullName);
                                                            }
                                                        }

                                                    }
                                                    catch (Exception exInnerCatchRetry3)
                                                    {
                                                        LogFile.Out("Error on Extracting Account Details on Retry 3: " + exInnerCatchRetry3.Message);
                                                        result = "Error";
                                                    }
                                                }
                                                else
                                                {
                                                    hasValues = false;
                                                    result = "";
                                                }
                                            }
                                            else
                                            {
                                                hasValues = false;
                                                result = "";
                                            }
                                        }
                                        else
                                        {
                                            hasValues = false;
                                            result = "";
                                        }
                                    }
                                    catch (Exception ex3)
                                    {
                                        LogFile.Out("Error encountered on getAccountDetails(): " + ex3.Message);
                                        LogFile.Out("Retry accessing to CRM after error - 3rd Retry");

                                        try
                                        {
                                            pSession.Destroy();

                                            pSession = EnterLoginCredentials();

                                            svcAccount.Url = pSession.GetURL();
                                            svcAccount.CookieContainer = pSession.GetCookieContainer();
                                            svcAccount.RequestEncoding = Encoding.UTF8;
                                            svcAccount.Timeout = 1200000;

                                            outputAccountQuery = svcAccount.AccountQueryPage(inputAccountQuery);

                                            if (outputAccountQuery != null)
                                            {
                                                if (outputAccountQuery.ListOfAccount.Account != null)
                                                {
                                                    if (outputAccountQuery.ListOfAccount.Account.Length >= 0)
                                                    {

                                                        if (outputAccountQuery.ListOfAccount.Account.Length >= 100)
                                                        {
                                                            hasValues = true;
                                                            iAccountCtr = iAccountCtr + 100;
                                                        }
                                                        else
                                                        {
                                                            hasValues = false;
                                                            iAccountCtr = iAccountCtr + outputAccountQuery.ListOfAccount.Account.Length;
                                                        }

                                                        //insertcode to save the records on the csv file
                                                        LogFile.Out(iAccountCtr + " account records has been extracted.");

                                                        try
                                                        {
                                                            for (Int32 idxAcct = 0; idxAcct < outputAccountQuery.ListOfAccount.Account.Length; idxAcct++)
                                                            {
                                                                String pAccountRowId = outputAccountQuery.ListOfAccount.Account[idxAcct].Id;
                                                                String pCreatedBy = outputAccountQuery.ListOfAccount.Account[idxAcct].CreatedByFullName;
                                                                DateTime pCreatedDate = outputAccountQuery.ListOfAccount.Account[idxAcct].CreatedDate;
                                                                String pModifiedBy = outputAccountQuery.ListOfAccount.Account[idxAcct].ModifiedBy.Substring(0, outputAccountQuery.ListOfAccount.Account[idxAcct].ModifiedBy.IndexOf(','));
                                                                DateTime pModifiedDate = outputAccountQuery.ListOfAccount.Account[idxAcct].ModifiedDate;

                                                                /*20170331-start of additional requests*/
                                                                String pAccountType = outputAccountQuery.ListOfAccount.Account[idxAcct].AccountType;
                                                                String pWRTerr = outputAccountQuery.ListOfAccount.Account[idxAcct].plWR_Terr;
                                                                String pACTerr = outputAccountQuery.ListOfAccount.Account[idxAcct].plAC_Terr;
                                                                String pDivisionName = outputAccountQuery.ListOfAccount.Account[idxAcct].DivisionName;
                                                                String pDivisionId = outputAccountQuery.ListOfAccount.Account[idxAcct].DivisionId;
                                                                String pGroupMember = outputAccountQuery.ListOfAccount.Account[idxAcct].IndexedPick0;
                                                                String pMainPhone = outputAccountQuery.ListOfAccount.Account[idxAcct].MainPhone;
                                                                String pBillToCountry = outputAccountQuery.ListOfAccount.Account[idxAcct].PrimaryBillToCountry;
                                                                String pShipToCountry = outputAccountQuery.ListOfAccount.Account[idxAcct].PrimaryShipToCountry;
                                                                /*20170331-end of additional requests*/

                                                                /*20170401-start of additional requests*/
                                                                String pOwnerId = outputAccountQuery.ListOfAccount.Account[idxAcct].OwnerId;
                                                                String pOwnerFullName = outputAccountQuery.ListOfAccount.Account[idxAcct].OwnerFullName;
                                                                /*20170401-end of additional requests*/

                                                                if (outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book != null)
                                                                {
                                                                    if (outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book.Length > 0)
                                                                    {
                                                                        LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ". Has " + outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book.Length + " associated books.");

                                                                        for (Int32 idxAcctBk = 0; idxAcctBk < outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book.Length; idxAcctBk++)
                                                                        {
                                                                            String pListOfBookName = outputAccountQuery.ListOfAccount.Account[idxAcct].ListOfBook.Book[idxAcctBk].BookName;

                                                                            LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ": " + pListOfBookName);
                                                                            result = LogFile.saveExtractedAccountDetails("Account", pAccountRowId, pListOfBookName, pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate,pAccountType,pWRTerr,pACTerr,pDivisionName,pDivisionId,pGroupMember,pMainPhone,pBillToCountry,pShipToCountry,pOwnerId,pOwnerFullName);
                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ". Has No Book Association.");
                                                                        result = LogFile.saveExtractedAccountDetails("Account", pAccountRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate,pAccountType, pWRTerr, pACTerr, pDivisionName, pDivisionId, pGroupMember, pMainPhone, pBillToCountry, pShipToCountry,pOwnerId,pOwnerFullName);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    LogFile.Out("Saving Associated Books for Account Row Id " + pAccountRowId + ". Has No Book Association.");
                                                                    result = LogFile.saveExtractedAccountDetails("Account", pAccountRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate,pAccountType, pWRTerr, pACTerr, pDivisionName, pDivisionId, pGroupMember, pMainPhone, pBillToCountry, pShipToCountry,pOwnerId,pOwnerFullName);
                                                                }
                                                            }

                                                        }
                                                        catch (Exception ex2)
                                                        {
                                                            LogFile.Out("Error on Extracting Account Details: " + ex2.Message);
                                                            result = "Error";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        hasValues = false;
                                                        result = "";
                                                    }
                                                }
                                                else
                                                {
                                                    hasValues = false;
                                                    result = "";
                                                }
                                            }
                                            else
                                            {
                                                hasValues = false;
                                                result = "";
                                            }
                                        }
                                        catch (Exception exInnerCatch)
                                        {
                                            LogFile.Out("Error encountered on getAccountDetails on exInnerCatch(): " + exInnerCatch.Message);
                                            LogFile.Out("Retry accessing to CRM after error - 2nd Retry");
                                        }
                                    }
                                }

                            }
                            catch (Exception exInnerCatch21)
                            {
                                LogFile.Out("Error on getAccountDetails on exInnerCatch21(): " + exInnerCatch21.Message);
                                hasValues = false;
                                result = "Error";
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LogFile.Out("Error on getAccountDetails(): " + ex.Message);
                hasValues = false;
                result="Error";
            }

            return result;

        }

        public static OnDemandSession EnterLoginCredentials()
        {
            LogFile.LogToFile("Initializing login credentials...");

            try
            {
                username = ConfigurationManager.AppSettings["username"].ToString();
                password = ConfigurationManager.AppSettings["password"].ToString();
                servername = ConfigurationManager.AppSettings["servername"].ToString();

                mSession = connectToCRMonDemand(servername, username, password);
            }
            catch (Exception ex)
            {
                LogFile.LogToFile("Error on Login Credentials --> " + ex.Message.ToString() + ". Retry login");

                try
                {
                    username = ConfigurationManager.AppSettings["username"].ToString();
                    password = ConfigurationManager.AppSettings["password"].ToString();

                    LogFile.LogToFile("Enter Username: " + username.ToString());
                    servername = ConfigurationManager.AppSettings["servername"].ToString();
                    LogFile.LogToFile("Enter Server Name: " + servername.ToString());

                    mSession = connectToCRMonDemand(servername, username, password);
                }
                catch (Exception ex1)
                {
                    LogFile.LogToFile("Error on Login Credentials --> " + ex1.Message.ToString());
                    mSession = null;
                }
            }

            return mSession;
        }

        public static OnDemandSession connectToCRMonDemand(String pServerName, String pUsername, String pPassword)
        {
            mSession = new OnDemandSession();

            try
            {
                //Retrieve Connecton Parameters
                mSession.server = pServerName;
                mSession.username = pUsername;
                mSession.password = pPassword;

                mSession.Establish();
            }
            catch (Exception ex)
            {
                LogFile.LogToFile("Error: connectToCRMonDemand --> " + ex.Message + ". Retry login");

                try
                {
                    //Retrieve Connecton Parameters
                    mSession.server = pServerName;
                    mSession.username = pUsername;
                    mSession.password = pPassword;

                    mSession.Establish();
                }
                catch (Exception ex1)
                {
                    LogFile.LogToFile("Error: connectToCRMonDemand --> " + ex1.Message);
                    mSession = null;
                }
            }

            return mSession;
        }

        
        }

}
