﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;

public class MailMaster
{
	public MailMaster(){}
    public void sendMail(string _host, string _subject,  string _emailaddressfrom, string _emailaddress, string _message)
    {
        string body = _message;
        SmtpClient client = new SmtpClient();
        client.Port = 25;
        client.Host = _host;
        MailMessage message = new MailMessage();
        message.From = new MailAddress(_emailaddressfrom);
        message.To.Add(_emailaddress);
        message.Subject = _subject; ;
        message.IsBodyHtml = false;
        message.Body = body;

        client.Send(message);
    }
    
}
