﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Configuration;
using System.IO;
using System.Data;
using System.Collections;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace CRM_Data_Extraction_Processor
{
    public class Program
    {
        public static OnDemandSession mSession;
        public static String username = "";
        public static String password = "";
        public static String servername = "";

        public static String recordType = "";
        public static String bookName = "";
        public static DateTime createdDateFrom = Convert.ToDateTime("1/1/2000");
        public static DateTime createdDateTo = Convert.ToDateTime("1/1/2000");
        public static String emailAddress = "";
        public static String notificationemailFrom = "";
        public static string notificationemailhost="";
        public static DateTime actualStartTimeStamp = Convert.ToDateTime("1/1/2000");
        public static DateTime actualEndTimeStamp = Convert.ToDateTime("1/1/2000");

        public CRM_Accounts cAccounts;
        //public static CRM_Contacts cContacts;

        public static void Main(string[] args)
        {

            notificationemailFrom = System.Configuration.ConfigurationManager.AppSettings["sendersFrom"];
            notificationemailhost = System.Configuration.ConfigurationManager.AppSettings["SMTPServer"];

            LogFile.Out("=== START: CRM_Data_Extraction_Processor ===");
            
            LogFile.Out("Initialize Parameters");
            InputValues();

            LogFile.Out("Proceed on Extracting Requested Set of Data");
            ProcessExtraction();

            mSession.Destroy();

            LogFile.Out("=== END: CRM_Data_Extraction_Processor ===");
        }

        public static void ProcessExtraction()
        {
            try
            {
                LogFile.Out("Initialize extraction of records. Please do not close this window...");

                if (recordType == "Account")
                {
                    processExtractionOfAccountRecords(bookName, createdDateFrom, createdDateTo.AddDays(1.00));
                }
                else if (recordType == "Contact")
                {
                    processExtractionOfContactRecords(bookName, createdDateFrom, createdDateTo.AddDays(1.00));
                }

                LogFile.Out("Extraction of record has been finished...");
            }
            catch (Exception ex)
            {
                LogFile.Out("Error on ProcessExtraction: " + ex.Message);
            }
        }

        static void InputValues()
        {
            String encodedBookOption="";
            String encodedRecordType="";
            String encodedDateFrom;
            String encodedDateTo;

            try
            {
                bool isValidRecordTypeOption = false;

                while (isValidRecordTypeOption == false)
                {
                    LogFile.Out("Input desired Record Type for Extraction:");
                    LogFile.Out("[Enter 'A' = Accounts , 'C' = Contacts]");
                    encodedRecordType = Console.ReadLine();

                    if (encodedRecordType == "A")
                    {
                        recordType = "Account";
                        LogFile.Out("Selected Record: " + recordType);
                        isValidRecordTypeOption = true;
                    }
                    else if (encodedRecordType == "C")
                    {
                        recordType = "Contact";
                        LogFile.Out("Selected Record: " + recordType);
                        isValidRecordTypeOption = true;
                    }
                    else
                    {
                        LogFile.Out("Invalid input on Record Type. Please input a valid Record Type."); 
                        isValidRecordTypeOption = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LogFile.Out(ex.Message);
            }

            try
            {
                bool isValidBookOption = false;

                while (isValidBookOption == false)
                {
                    LogFile.Out("Input desired Book Selector Option:");
                    LogFile.Out("[Enter '0' =All Books , '1' = To Input a Specific Book Name]");
                    encodedBookOption = Console.ReadLine();
                    LogFile.Out(encodedBookOption);

                    if (encodedBookOption == "0")
                    {
                        bookName = "";
                        LogFile.Out("Selected Book Selector Option: All Books");
                        isValidBookOption = true;
                    }
                    else if (encodedBookOption == "1")
                    {
                        Console.WriteLine("Input desired Book Name: ");
                        bookName = Console.ReadLine();
                        LogFile.Out("Selected Book Name: " + bookName);
                        isValidBookOption = true;
                    }
                    else
                    {
                        LogFile.Out("Invalid Book Selector Option: provided. Please input a valid Book Selector Option.");
                        isValidBookOption = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LogFile.Out(ex.Message);
            }

            try
            {
                bool isValidDate = false;

                while (isValidDate == false)
                {
                    LogFile.Out("Input desired Creation Date Range in MM/dd/yyyy format:");
                    LogFile.Out("Input Created Date From: ");
                    encodedDateFrom = Console.ReadLine();
                    LogFile.LogToFile(encodedDateFrom);

                    LogFile.Out("Input Created Date To: ");
                    encodedDateTo = Console.ReadLine();
                    LogFile.LogToFile(encodedDateTo);

                    if (Convert.ToDateTime(encodedDateTo) >= Convert.ToDateTime(encodedDateFrom))
                    {
                        createdDateFrom = Convert.ToDateTime(encodedDateFrom);
                        createdDateTo = Convert.ToDateTime(encodedDateTo);
                        isValidDate = true;
                    }
                    else
                    {
                        LogFile.Out("Invalid Date Range provided. Please input a valid date range");
                        isValidDate = false;
                    }
                }
           
            }
            catch (Exception ex)
            {
                LogFile.Out(ex.Message);
            }

            try
            {
                bool isValidEmail = false;

                LogFile.Out("Input your email address: ");
                emailAddress = Console.ReadLine();
                LogFile.LogToFile(emailAddress);

                while (isValidEmail == false)
                {
                    if (emailAddress != "")
                    {
                        isValidEmail = true;
                        LogFile.Out("Requester with email address " + emailAddress + " will be notified once the extraction process has been completed.");
                    }
                    else
                    {
                        isValidEmail = false;
                        LogFile.Out("Email address should not be null or blank. Please input a valid email address.");
                    }
                }
            }
            catch (Exception ex)
            {
                LogFile.Out(ex.Message);
            }
        }

        public static String processExtractionOfContactRecords(String pBookName, DateTime pCreatedDateFrom, DateTime pCreatedDateTo)
        {
            String result = "";
            String extractedFilenameAndPath = "";

            String emailBody = "";
            String emailSubject = "";

            try
            {
                mSession = EnterLoginCredentials();

                try
                {
                    CRM_Contacts cContacts = new CRM_Contacts();
                    MailMaster cMail = new MailMaster();

                    actualStartTimeStamp = DateTime.Now;

                    //below method should return the filename of the generated file
                    extractedFilenameAndPath = cContacts.getContactDetails(mSession, bookName, pCreatedDateFrom, pCreatedDateTo);

                    actualEndTimeStamp = DateTime.Now;

                    LogFile.Out("Total Elapse Time: " + (actualEndTimeStamp - actualStartTimeStamp));

                    //send an email to the extraction requestor, include the filename and path of the file 
                    LogFile.Out("Email is now being prepared to be sent to the requester...");
                    if (extractedFilenameAndPath != "Error" && extractedFilenameAndPath != "")
                    {
                        emailBody = "Dear Requester,\n\nPlease find the extracted files at " + extractedFilenameAndPath + " from " + System.Environment.MachineName + ".";
                        emailBody += "\n\nBelow are the parameters that you requested:\nRecord Type:Contact\nBookName:" + pBookName + "\nCreated Date From:" + pCreatedDateFrom.ToShortDateString() + "\nCreated Date To:" + pCreatedDateTo.AddDays(-1.00).ToShortDateString() + "\n\nStart Time of Extraction:" + actualStartTimeStamp + "\n\nEnd Time of Extraction:" + actualEndTimeStamp + "\n\nTotal Elapse Time: " + (actualEndTimeStamp - actualStartTimeStamp) + "\n\nThanks!";
                    }
                    else
                    {
                        emailBody = "Dear Requester,\n\nNo results found on the parameters that you requested.";
                        emailBody += "\n\nBelow are the parameters that you requested:\nRecord Type:Contact\nBookName:" + pBookName + "\nCreated Date From:" + pCreatedDateFrom.ToShortDateString() + "\nCreated Date To:" + pCreatedDateTo.AddDays(-1.00).ToShortDateString() + "\n\nStart Time of Extraction:" + actualStartTimeStamp + "\n\nEnd Time of Extraction:" + actualEndTimeStamp + "\n\nTotal Elapse Time: " + (actualEndTimeStamp - actualStartTimeStamp) + "\n\nThanks!";
                    }

                    emailSubject = "[CRMDQ]For Your Reference: Request for Extraction of Contact Data Results";

                    cMail.sendMail(notificationemailhost, emailSubject, notificationemailFrom, emailAddress, emailBody);

                    LogFile.Out("Email notification has been successfully sent to the requester...");
                    result = "Success";
                }
                catch (Exception ex)
                {
                    LogFile.Out("Error on processExtractionOfContactRecords(): " + ex.Message);
                    result = "Error";
                }
            }
            catch (Exception ex)
            {
                LogFile.Out("Error on processExtractionOfContactRecords(): " + ex.Message);
                result = "Error";
            }

            return result;

        }

        /*
         * Changes made by RDVillamor
         * 1. 20170301 - Add start and end time of processing, include it on the email notification
         */

        public static String processExtractionOfAccountRecords(String pBookName, DateTime pCreatedDateFrom, DateTime pCreatedDateTo)
        {
            String result = "";
            String extractedFilenameAndPath = "";
            
            String emailBody = "";
            String emailSubject = "";

            try
            {
                mSession = EnterLoginCredentials();

                try
                {
                    CRM_Accounts cAccounts = new CRM_Accounts();
                    MailMaster cMail = new MailMaster();

                    actualStartTimeStamp = DateTime.Now;

                    //below method should return the filename of the generated file
                    extractedFilenameAndPath = cAccounts.getAccountDetails(mSession, bookName, pCreatedDateFrom, pCreatedDateTo);

                    actualEndTimeStamp = DateTime.Now;

                    LogFile.Out("Total Elapse Time: " + (actualEndTimeStamp - actualStartTimeStamp));

                    //send an email to the extraction requestor, include the filename and path of the file 
                    LogFile.Out("Email is now being prepared to be sent to the requester...");
                    if (extractedFilenameAndPath != "Error" && extractedFilenameAndPath !="")
                    {
                        emailBody = "Dear Requester,\n\nPlease find the extracted files at " + extractedFilenameAndPath + " from " + System.Environment.MachineName + ".";
                        emailBody += "\n\nBelow are the parameters that you requested:\nRecord Type:Account\nBookName:" + pBookName + "\nCreated Date From:" + pCreatedDateFrom.ToShortDateString() + "\nCreated Date To:" + pCreatedDateTo.AddDays(-1.00).ToShortDateString() + "\n\nStart Time of Extraction:" + actualStartTimeStamp + "\n\nEnd Time of Extraction:" + actualEndTimeStamp + "\n\nTotal Elapse Time: " + (actualEndTimeStamp - actualStartTimeStamp) + "\n\nThanks!";
                    }
                    else
                    {
                        emailBody = "Dear Requester,\n\nNo results found on the parameters that you requested.";
                        emailBody += "\n\nBelow are the parameters that you requested:\nRecord Type:Account\nBookName:" + pBookName + "\nCreated Date From:" + pCreatedDateFrom.ToShortDateString() + "\nCreated Date To:" + pCreatedDateTo.AddDays(-1.00).ToShortDateString() + "\n\nStart Time of Extraction:" + actualStartTimeStamp + "\n\nEnd Time of Extraction:" + actualEndTimeStamp + "\n\nTotal Elapse Time: " + (actualEndTimeStamp - actualStartTimeStamp) + "\n\nThanks!";
                    }

                    emailSubject = "[CRMDQ]For Your Reference: Request for Extraction of Accounts Data Results";

                    cMail.sendMail(notificationemailhost, emailSubject, notificationemailFrom, emailAddress, emailBody);

                    LogFile.Out("Email notification has been successfully sent to the requester...");
                    result = "Success";
                }
                catch (Exception ex)
                {
                    LogFile.Out("Error on processExtractionOfAccountRecords(): " + ex.Message);
                    result = "Error";
                }
            }
            catch (Exception ex)
            {
                LogFile.Out("Error on processExtractionOfAccountRecords(): " + ex.Message);
                result = "Error";
            }

            return result;
        }

        

        public static OnDemandSession EnterLoginCredentials()
        {
            LogFile.LogToFile("Initializing login credentials...");

            try
            {
                username = ConfigurationManager.AppSettings["username"].ToString();
                password = ConfigurationManager.AppSettings["password"].ToString();
                servername = ConfigurationManager.AppSettings["servername"].ToString();

                mSession = connectToCRMonDemand(servername, username, password);
            }
            catch (Exception ex)
            {
                LogFile.LogToFile("Error on Login Credentials --> " + ex.Message.ToString() + ". Retry login");

                try
                {
                    username = ConfigurationManager.AppSettings["username"].ToString();
                    password = ConfigurationManager.AppSettings["password"].ToString();

                    LogFile.LogToFile("Enter Username: " + username.ToString());
                    servername = ConfigurationManager.AppSettings["servername"].ToString();
                    LogFile.LogToFile("Enter Server Name: " + servername.ToString());

                    mSession = connectToCRMonDemand(servername, username, password);
                }
                catch (Exception ex1)
                {
                    LogFile.LogToFile("Error on Login Credentials --> " + ex1.Message.ToString());
                    mSession = null;
                }
            }

            return mSession;
        }

        public static OnDemandSession connectToCRMonDemand(String pServerName, String pUsername, String pPassword)
        {
            mSession = new OnDemandSession();

            try
            {
                //Retrieve Connecton Parameters
                mSession.server = pServerName;
                mSession.username = pUsername;
                mSession.password = pPassword;

                mSession.Establish();
            }
            catch (Exception ex)
            {
                LogFile.LogToFile("Error: connectToCRMonDemand --> " + ex.Message + ". Retry login");

                try
                {
                    //Retrieve Connecton Parameters
                    mSession.server = pServerName;
                    mSession.username = pUsername;
                    mSession.password = pPassword;

                    mSession.Establish();
                }
                catch (Exception ex1)
                {
                    LogFile.LogToFile("Error: connectToCRMonDemand --> " + ex1.Message);
                    mSession = null;
                }
            }

            return mSession;
        }

        public static String moveFileToProcessedFolder(String pSourceFile)
        {
            LogFile.LogToFile("Start moving " + pSourceFile + " to ARCHIVED folder...");

            String result = "";

            String strFilenameWithExtension = Path.GetFileName(pSourceFile);
            String strFilenameWithoutExtension = Path.GetFileNameWithoutExtension(pSourceFile);

            String targetFolder = ConfigurationManager.AppSettings["ProcessedFilesFolder"].ToString();
            String sourceFile = System.IO.Path.Combine("Test.csv", strFilenameWithExtension);
            String destFile = System.IO.Path.Combine(targetFolder, strFilenameWithExtension);
            try
            {
                if (!System.IO.Directory.Exists(targetFolder))
                {
                    System.IO.Directory.CreateDirectory(targetFolder);
                }

                System.IO.File.Move(sourceFile, destFile);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error on moveProcessedFileToArchivedFolder: --> " + ex.Message);
                LogFile.LogToFile("Error on moveProcessedFileToArchivedFolder: --> " + ex.Message);
                result = "Error";
            }

            LogFile.LogToFile("Ended moving " + pSourceFile + " to ARCHIVED folder...");

            return result;
        }
    }
}
