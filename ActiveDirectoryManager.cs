﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.WebServices.Data;
using System.Collections;
using System.Text.RegularExpressions;
using System.Net;


namespace CRM_Data_Extraction_Processor
{
    public class ActiveDirectoryManager
    {
        public List<EmailMessage> getAllMails(string _username, string _userpassword, string _emailaddress, string _emailServer)
        {
            ////commented by RDVillamor - 20161026
            ////ExchangeService _service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
            //ExchangeService _service = new ExchangeService(ExchangeVersion.Exchange2010);
            //_service.Credentials = new WebCredentials(_username, _userpassword);
            //_service.AutodiscoverUrl(_emailaddress);

            // exchangeVersion should be ExchangeVersion.Exchange2013_SP1
            ExchangeService _service = new ExchangeService(ExchangeVersion.Exchange2010);
            _service.TraceEnabled = true;
            _service.Url = new Uri(_emailServer);
            _service.Credentials = new WebCredentials(_emailaddress,_userpassword);

            ItemView unreaditemsview = new ItemView(100);
            ItemView readitemsview = new ItemView(100);

            SearchFilter unreadfilter = new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, false);
            SearchFilter readfilter = new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, true);
            FindItemsResults<Item> unreaditems = _service.FindItems(WellKnownFolderName.Inbox, unreadfilter, unreaditemsview);
            FindItemsResults<Item> readitems = _service.FindItems(WellKnownFolderName.Inbox, readfilter, readitemsview);

            List<EmailMessage> messages = new List<EmailMessage>();

            
                foreach (EmailMessage item in unreaditems)
                    messages.Add(item);
                foreach (EmailMessage item in readitems)
                    messages.Add(item);


            if (messages.Count > 0)
                _service.LoadPropertiesForItems(messages, PropertySet.FirstClassProperties);
           

            return messages;
        }
        public void sendEmail(string _username, string _userpassword, string _emailaddress, string _subject, string _recipient, string _message, string _emailServer)
        {
            ExchangeService _service = new ExchangeService(ExchangeVersion.Exchange2010);
            _service.TraceEnabled = true;
            _service.Url = new Uri(_emailServer);
            _service.Credentials = new WebCredentials(_emailaddress, _userpassword);

            EmailMessage newMessage = new EmailMessage(_service);
            newMessage.Subject = _subject;
            newMessage.ToRecipients.Add(_recipient);
            newMessage.Body = _message;
            newMessage.Body.BodyType = BodyType.Text;
            newMessage.Body.Text = _message;

            newMessage.SendAndSaveCopy();
        }
        public string getMessageBody(MessageBody body)
        {
            string ret = "";
            if (body.BodyType == BodyType.Text)
                ret = body.ToString();
            else
            {
                string htmlContent = body.ToString();
                string convertedText = "";
                int bodyStartindex = htmlContent.IndexOf("<body");
                int bodyEndIndex = htmlContent.IndexOf("</body>");

                // get the string enclosed in body
                String bodyStringTemp = "";
                if (bodyStartindex < 0)
                    bodyStringTemp = htmlContent;
                else
                    bodyStringTemp = htmlContent.Substring(bodyStartindex, bodyEndIndex - bodyStartindex);

                // remove the start body tag
                int closingTagIndex = 0;
                for (int i = 0; i < bodyStringTemp.Length - 1; i++)
                {

                    //if (bodyStringTemp.Substring(i, i + 1).equalsIgnoreCase(">"))
                    if (String.Equals(bodyStringTemp.Substring(i, 1).ToUpper(),(">")) == true)
                    {
                        closingTagIndex = i;
                        i = bodyStringTemp.Length;
                    }
                }

                // get the enclosing text
                string bodyString = bodyStringTemp.Substring(closingTagIndex + 1);
                bodyString = bodyString.Replace(Environment.NewLine, String.Empty);
                //bodyString = bodyString.Replace("<o:p></o:p>", "\n");
                //bodyString = bodyString.Replace("<o:p></o:p>", "#########NEXT LINE##########");
                bodyString = bodyString.Replace("</p>", "\n");
                bodyString = bodyString.Replace("<br>", "\n");
                bodyString = bodyString.Replace("<br />", "\n");
                bodyString = bodyString.Replace("<br/>", "\n");
                bodyString = bodyString.Replace("</span>", String.Empty);
                bodyString = Regex.Replace(bodyString, "<.*?>", String.Empty);
                bodyString = System.Web.HttpUtility.HtmlDecode(bodyString);
                //bodyString = bodyString.Replace("&nbsp;", " ");
                //bodyString = bodyString.Replace("&amp;", "&");

                convertedText = bodyString.Trim();
                ret = convertedText;

            }
            return ret;
        }
    }
}
