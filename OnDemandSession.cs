﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace CRM_Data_Extraction_Processor
{
    public class OnDemandSession
    {
        public string server;
        public string username;
        public string password;
        public string port;
        public string sessionId;
        public Cookie cookie;

        private string httpBase = "https://";	// Use https for production implementations
        public void Establish()
        {
            // Check that username and password have been specified
            CheckUsernamePassword();

            // create a container for an HTTP request
            string userl = GetLogInURL();
            //System.Windows.Forms.MessageBox.Show(userl);
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(userl);
            //System.Windows.Forms.MessageBox.Show(req.Address.AbsoluteUri);
            //req.Method = "POST";

            // username and password are passed as HTTP headers
            req.Headers.Add("UserName", username);
            req.Headers.Add("Password", password);
            req.Headers.Add("SessionType", "Stateless");

            // cookie container has to be added to request in order to 
            // retrieve the cookie from the response. 
            req.CookieContainer = new CookieContainer();

            // make the HTTP call
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                // store cookie for later...
                cookie = resp.Cookies["JSESSIONID"];
                if (cookie == null)
                {
                    throw new Exception("No JSESSIONID cookie found in log-in response!");
                }
                sessionId = cookie.Value;
            }
        }

        public void Destroy()
        {
            // create a container for an HTTP request
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(GetLogOffURL());

            // reuse the cookie that was received at Login
            req.CookieContainer = new CookieContainer();
            req.CookieContainer.Add(cookie);
            req.Headers.Add("SessionType", "None");

            // make the HTTP call
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            if (resp.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Logging off failed!");
            }
            // forget current session id
            sessionId = null;
        }

        public CookieContainer GetCookieContainer()
        {
            // Returns a CookieContainer that can be used in a web services call
            if (sessionId == null)
            {
                throw new Exception("No session has been established!");
            }
            CookieContainer cc = new CookieContainer();
            Cookie sCookie = new Cookie("JSESSIONID", sessionId);
            sCookie.Domain = ".siebel.com";
            cc.Add(sCookie);

            return cc;
        }

        /*
         * 20160503 / RDVillamor
         * 1. Enhanced to add retry/reestablish if sessionId was found to be null
         */

        public string GetURL()
        {
            if (sessionId == null)
            {
                Establish();

                if (sessionId == null)
                {
                    throw new Exception("No session has been established!");
                }
            }
            //CheckServerPort();
            return httpBase + server + "/Services/Integration;jsessionid=" + sessionId;
        }
        private string GetLogInURL()
        {
            //CheckServerPort();

            //return httpBase + server + ":" + "/Services/Integration?command=login";
            return httpBase + server + "/Services/Integration?command=login";
            //return httpBase + server + ":" + port + "/OnDemand/logon.jsp";

        }
        private string GetLogOffURL()
        {
            //CheckServerPort();
            return httpBase + server + "/Services/Integration?command=logoff";
        }

        private void CheckUsernamePassword()
        {
            if (username == null)
            {
                throw new Exception("Username not specified!");
            }
            if (password == null)
            {
                throw new Exception("Password not specified!");
            }
        }
    }
}
