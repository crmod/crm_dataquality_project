﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;

namespace CRM_Data_Extraction_Processor
{
    public class CRM_Contacts
    {
        public static OnDemandSession mSession;
        public static String username = "";
        public static String password = "";
        public static String servername = "";

        /*
         * Changes Made:
         * 1. 20170301 - Add Created By, Created Date, Modified By, Modified Date fields on the output dataset
         * 2. 20170331 - Add the following field on the output file: Primary Account Id, Contact Type, Group Member, Division, Contact Country
         * 3. 20170403 - Add OwnerId and OwnerFullName on the queried fields
         */
        public String getContactDetails(OnDemandSession pSession, String pBookName, DateTime pCreatedDateFrom, DateTime pCreatedDateTo)
        {
            Int32 iContactCtr = 0;
            bool hasValues = true;
            String result = "";

            try
            {
                while (hasValues == true)
                {
                    WSOD_Contact_v2.Contact svcContact = new WSOD_Contact_v2.Contact();
                    WSOD_Contact_v2.ContactQueryPage_Input inputContactQuery = new WSOD_Contact_v2.ContactQueryPage_Input();
                    WSOD_Contact_v2.ContactQueryPage_Output outputContactQuery = new WSOD_Contact_v2.ContactQueryPage_Output();

                    inputContactQuery.IncludeSubBooks = "true";

                    if(pBookName!="")
                    {
                        inputContactQuery.BookName = pBookName;
                    }
                    else
                    {
                        inputContactQuery.ViewMode = "Broadest";
                    }

                    inputContactQuery.ListOfContact = new WSOD_Contact_v2.ListOfContactQuery();
                    inputContactQuery.ListOfContact.pagesize = "100";
                    inputContactQuery.ListOfContact.startrownum = iContactCtr.ToString();

                    inputContactQuery.ListOfContact.Contact = new WSOD_Contact_v2.ContactQuery();
                    inputContactQuery.ListOfContact.Contact.Id = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.ContactFirstName = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.ContactLastName = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.ContactEmail = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.NeverEmail = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.Owner = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.AccountName = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.AccountId = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.CreatedDate = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.CreatedDateExt = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.CreatedByFullName = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.ModifiedBy = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.ModifiedDate = new WSOD_Contact_v2.queryType();

                    /*20170331 - start of additional fields*/
                    inputContactQuery.ListOfContact.Contact.AccountId = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.AccountName = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.ContactType = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.DivisionId = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.DivisionName = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.AlternateCountry = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.IndexedPick0 = new WSOD_Contact_v2.queryType();
                    /*20170331 - end of additional fields*/

                    /*20170403 - start of additional fields*/
                    inputContactQuery.ListOfContact.Contact.OwnerId = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.OwnerFullName = new WSOD_Contact_v2.queryType();
                    /*20170403 - end of additional fields*/

                    inputContactQuery.ListOfContact.Contact.ListOfBook = new WSOD_Contact_v2.ListOfBookQuery();
                    inputContactQuery.ListOfContact.Contact.ListOfBook.Book = new WSOD_Contact_v2.BookQuery();
                    inputContactQuery.ListOfContact.Contact.ListOfBook.pagesize = "100";
                    inputContactQuery.ListOfContact.Contact.ListOfBook.startrownum = "0";
                    inputContactQuery.ListOfContact.Contact.ListOfBook.Book.BookName = new WSOD_Contact_v2.queryType();
                    inputContactQuery.ListOfContact.Contact.ListOfBook.Book.BookId = new WSOD_Contact_v2.queryType();

                    inputContactQuery.ListOfContact.Contact.searchspec = "([CreatedDate] >= '" + pCreatedDateFrom.ToShortDateString() + "' AND [CreatedDate] <= '" + pCreatedDateTo.ToShortDateString() + "')";
                    inputContactQuery.ListOfContact.Contact.CreatedDate.sortorder = "ASC";

                    try
                    {
                        svcContact.Url = pSession.GetURL();
                        svcContact.CookieContainer = pSession.GetCookieContainer();
                        svcContact.RequestEncoding = Encoding.UTF8;
                        svcContact.Timeout = 1200000;

                        outputContactQuery = svcContact.ContactQueryPage(inputContactQuery);

                        if (outputContactQuery != null)
                        {
                            if (outputContactQuery.ListOfContact.Contact != null)
                            {
                                if (outputContactQuery.ListOfContact.Contact.Length > 0)
                                {
                                    if (outputContactQuery.ListOfContact.Contact.Length >= 100)
                                    {
                                        hasValues = true;
                                        iContactCtr = iContactCtr + 100;
                                    }
                                    else
                                    {
                                        hasValues = false;
                                        iContactCtr = iContactCtr + outputContactQuery.ListOfContact.Contact.Length;
                                    }

                                    LogFile.Out(iContactCtr + " contact records will be extracted.");

                                    try
                                    {
                                        for (Int32 idxContact = 0; idxContact < outputContactQuery.ListOfContact.Contact.Length; idxContact++)
                                        {
                                            String pContactRowId = outputContactQuery.ListOfContact.Contact[idxContact].Id;
                                            String pCreatedBy = outputContactQuery.ListOfContact.Contact[idxContact].CreatedByFullName;
                                            DateTime pCreatedDate = outputContactQuery.ListOfContact.Contact[idxContact].CreatedDate;
                                            String pModifiedBy = outputContactQuery.ListOfContact.Contact[idxContact].ModifiedBy.Substring(0, outputContactQuery.ListOfContact.Contact[idxContact].ModifiedBy.IndexOf(','));
                                            DateTime pModifiedDate = outputContactQuery.ListOfContact.Contact[idxContact].ModifiedDate;
                                            
                                            /*20170331 - start of additional fields*/
                                            String pAccountId = outputContactQuery.ListOfContact.Contact[idxContact].AccountId;
                                            String pAccountName = outputContactQuery.ListOfContact.Contact[idxContact].AccountName;
                                            String pContactType = outputContactQuery.ListOfContact.Contact[idxContact].ContactType;
                                            String pDivisionId = outputContactQuery.ListOfContact.Contact[idxContact].DivisionId;
                                            String pDivisionName = outputContactQuery.ListOfContact.Contact[idxContact].DivisionName;
                                            String pContactCountry = outputContactQuery.ListOfContact.Contact[idxContact].AlternateCountry;
                                            String pGroupMember = outputContactQuery.ListOfContact.Contact[idxContact].IndexedPick0;
                                            /*20170331 - end of additional fields*/

                                            /*20170403 - start of additional field*/
                                            String pOwnerId = outputContactQuery.ListOfContact.Contact[idxContact].OwnerId;
                                            String pOwnerFullName = outputContactQuery.ListOfContact.Contact[idxContact].OwnerFullName;
                                            /*20170403 - End of additional field*/

                                            if (outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book != null)
                                            {
                                                if (outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book.Length > 0)
                                                {
                                                    LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ". Has " + outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book.Length + " associated books.");

                                                    for (Int32 idxContactBk = 0; idxContactBk < outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book.Length; idxContactBk++)
                                                    {
                                                        String pListOfBookName = outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book[idxContactBk].BookName;

                                                        LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ": " + pListOfBookName);
                                                        result = LogFile.saveExtractedContactDetails("Contact", pContactRowId, pListOfBookName, pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate,pAccountId,pAccountName,pContactType,pDivisionId,pDivisionName,pContactCountry,pGroupMember,pOwnerId,pOwnerFullName);
                                                    }
                                                }
                                                else
                                                {
                                                    LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ". Has No Book Association.");
                                                    result = LogFile.saveExtractedContactDetails("Contact", pContactRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountId, pAccountName, pContactType, pDivisionId, pDivisionName, pContactCountry, pGroupMember,pOwnerId, pOwnerFullName);
                                                }
                                            }
                                            else
                                            {
                                                LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ". Has No Book Association.");
                                                result = LogFile.saveExtractedContactDetails("Contact", pContactRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountId, pAccountName, pContactType, pDivisionId, pDivisionName, pContactCountry, pGroupMember, pOwnerId, pOwnerFullName);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LogFile.Out("Error on Extracting Contact Details: " + ex.Message);
                                        hasValues = false;
                                        result = "Error";
                                    }
                                }
                                else
                                {
                                    hasValues = false;
                                    result = "";
                                }
                            }
                            else
                            {
                                hasValues = false;
                                result = "";
                            }
                        }
                        else
                        {
                            hasValues = false;
                            result = "";
                        }
                    }
                    catch (Exception ex)
                    {
                        LogFile.Out("Error encountered on getContactDetails(): " + ex.Message);
                        LogFile.Out("Retry accessing to CRM after error - Retry 1");

                        try
                        {
                            pSession.Destroy();

                            pSession = EnterLoginCredentials();

                            svcContact.Url = pSession.GetURL();
                            svcContact.CookieContainer = pSession.GetCookieContainer();
                            svcContact.RequestEncoding = Encoding.UTF8;
                            svcContact.Timeout = 1200000;

                            outputContactQuery = svcContact.ContactQueryPage(inputContactQuery);

                            if (outputContactQuery != null)
                            {
                                if (outputContactQuery.ListOfContact.Contact != null)
                                {
                                    if (outputContactQuery.ListOfContact.Contact.Length > 0)
                                    {
                                        if (outputContactQuery.ListOfContact.Contact.Length >= 100)
                                        {
                                            hasValues = true;
                                            iContactCtr = iContactCtr + 100;
                                        }
                                        else
                                        {
                                            hasValues = false;
                                            iContactCtr = iContactCtr + outputContactQuery.ListOfContact.Contact.Length;
                                        }

                                        LogFile.Out(iContactCtr + " contact records will be extracted.");

                                        try
                                        {
                                            for (Int32 idxContact = 0; idxContact < outputContactQuery.ListOfContact.Contact.Length; idxContact++)
                                            {
                                                String pContactRowId = outputContactQuery.ListOfContact.Contact[idxContact].Id;
                                                String pCreatedBy = outputContactQuery.ListOfContact.Contact[idxContact].CreatedByFullName;
                                                DateTime pCreatedDate = outputContactQuery.ListOfContact.Contact[idxContact].CreatedDate;
                                                String pModifiedBy = outputContactQuery.ListOfContact.Contact[idxContact].ModifiedBy.Substring(0, outputContactQuery.ListOfContact.Contact[idxContact].ModifiedBy.IndexOf(','));
                                                DateTime pModifiedDate = outputContactQuery.ListOfContact.Contact[idxContact].ModifiedDate;

                                                /*20170331 - start of additional fields*/
                                                String pAccountId = outputContactQuery.ListOfContact.Contact[idxContact].AccountId;
                                                String pAccountName = outputContactQuery.ListOfContact.Contact[idxContact].AccountName;
                                                String pContactType = outputContactQuery.ListOfContact.Contact[idxContact].ContactType;
                                                String pDivisionId = outputContactQuery.ListOfContact.Contact[idxContact].DivisionId;
                                                String pDivisionName = outputContactQuery.ListOfContact.Contact[idxContact].DivisionName;
                                                String pContactCountry = outputContactQuery.ListOfContact.Contact[idxContact].AlternateCountry;
                                                String pGroupMember = outputContactQuery.ListOfContact.Contact[idxContact].IndexedPick0;
                                                /*20170331 - end of additional fields*/

                                                /*20170403 - start of additional field*/
                                                String pOwnerId = outputContactQuery.ListOfContact.Contact[idxContact].OwnerId;
                                                String pOwnerFullName = outputContactQuery.ListOfContact.Contact[idxContact].OwnerFullName;
                                                /*20170403 - End of additional field*/

                                                if (outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book != null)
                                                {
                                                    if (outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book.Length > 0)
                                                    {
                                                        LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ". Has " + outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book.Length + " associated books.");

                                                        for (Int32 idxContactBk = 0; idxContactBk < outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book.Length; idxContactBk++)
                                                        {
                                                            String pListOfBookName = outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book[idxContactBk].BookName;

                                                            LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ": " + pListOfBookName);
                                                            result = LogFile.saveExtractedContactDetails("Contact", pContactRowId, pListOfBookName, pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountId, pAccountName, pContactType, pDivisionId, pDivisionName, pContactCountry, pGroupMember, pOwnerId, pOwnerFullName);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ". Has No Book Association.");
                                                        result = LogFile.saveExtractedContactDetails("Contact", pContactRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountId, pAccountName, pContactType, pDivisionId, pDivisionName, pContactCountry, pGroupMember, pOwnerId, pOwnerFullName);
                                                    }
                                                }
                                                else
                                                {
                                                    LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ". Has No Book Association.");
                                                    result = LogFile.saveExtractedContactDetails("Contact", pContactRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountId, pAccountName, pContactType, pDivisionId, pDivisionName, pContactCountry, pGroupMember, pOwnerId, pOwnerFullName);
                                                }
                                            }
                                        }
                                        catch (Exception ex2)
                                        {
                                            LogFile.Out("Error on Extracting Contact Details: " + ex2.Message);
                                            hasValues = false;
                                            result = "Error";
                                        }
                                    }
                                    else
                                    {
                                        hasValues = false;
                                        result = "";
                                    }
                                }
                                else
                                {
                                    hasValues = false;
                                    result = "";
                                }
                            }
                            else
                            {
                                hasValues = false;
                                result = "";
                            }
                        }
                        catch (Exception exInnerCatch2)
                        {
                            LogFile.Out("Error encountered on getContactDetails(): " + exInnerCatch2.Message);
                            LogFile.Out("Retry accessing to CRM after error - Retry 2");

                            pSession.Destroy();

                            LogFile.Out("Processing of query will rest for 90 seconds. Please wait...");
                            System.Threading.Thread.Sleep(90000);
                            LogFile.Out("Processing will now resume.");

                            pSession = EnterLoginCredentials();

                            try
                            {
                                svcContact = new WSOD_Contact_v2.Contact();
                                inputContactQuery = new WSOD_Contact_v2.ContactQueryPage_Input();
                                outputContactQuery = new WSOD_Contact_v2.ContactQueryPage_Output();

                                while (hasValues == true)
                                {
                                    inputContactQuery.IncludeSubBooks = "true";

                                    if (pBookName != "")
                                    {
                                        inputContactQuery.BookName = pBookName;
                                    }
                                    else
                                    {
                                        inputContactQuery.ViewMode = "Broadest";
                                    }

                                    inputContactQuery.ListOfContact = new WSOD_Contact_v2.ListOfContactQuery();
                                    inputContactQuery.ListOfContact.pagesize = "100";
                                    inputContactQuery.ListOfContact.startrownum = iContactCtr.ToString();

                                    inputContactQuery.ListOfContact.Contact = new WSOD_Contact_v2.ContactQuery();
                                    inputContactQuery.ListOfContact.Contact.Id = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.ContactFirstName = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.ContactLastName = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.ContactEmail = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.NeverEmail = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.Owner = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.AccountName = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.AccountId = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.CreatedDate = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.CreatedDateExt = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.CreatedByFullName = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.ModifiedBy = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.ModifiedDate = new WSOD_Contact_v2.queryType();

                                    /*20170331 - start of additional fields*/
                                    inputContactQuery.ListOfContact.Contact.AccountId = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.AccountName = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.ContactType = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.DivisionId = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.DivisionName = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.AlternateCountry = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.IndexedPick0 = new WSOD_Contact_v2.queryType();
                                    /*20170331 - end of additional fields*/

                                    /*20170403 - start of additional fields*/
                                    inputContactQuery.ListOfContact.Contact.OwnerId = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.OwnerFullName = new WSOD_Contact_v2.queryType();
                                    /*20170403 - end of additional fields*/

                                    inputContactQuery.ListOfContact.Contact.ListOfBook = new WSOD_Contact_v2.ListOfBookQuery();
                                    inputContactQuery.ListOfContact.Contact.ListOfBook.Book = new WSOD_Contact_v2.BookQuery();
                                    inputContactQuery.ListOfContact.Contact.ListOfBook.pagesize = "100";
                                    inputContactQuery.ListOfContact.Contact.ListOfBook.startrownum = "0";
                                    inputContactQuery.ListOfContact.Contact.ListOfBook.Book.BookName = new WSOD_Contact_v2.queryType();
                                    inputContactQuery.ListOfContact.Contact.ListOfBook.Book.BookId = new WSOD_Contact_v2.queryType();

                                    inputContactQuery.ListOfContact.Contact.searchspec = "([CreatedDate] >= '" + pCreatedDateFrom.ToShortDateString() + "' AND [CreatedDate] <= '" + pCreatedDateTo.ToShortDateString() + "')";
                                    inputContactQuery.ListOfContact.Contact.CreatedDate.sortorder = "ASC";

                                    try
                                    {
                                        svcContact.Url = pSession.GetURL();
                                        svcContact.CookieContainer = pSession.GetCookieContainer();
                                        svcContact.RequestEncoding = Encoding.UTF8;
                                        svcContact.Timeout = 1200000;

                                        outputContactQuery = svcContact.ContactQueryPage(inputContactQuery);

                                        if (outputContactQuery != null)
                                        {
                                            if (outputContactQuery.ListOfContact.Contact != null)
                                            {
                                                if (outputContactQuery.ListOfContact.Contact.Length > 0)
                                                {
                                                    if (outputContactQuery.ListOfContact.Contact.Length >= 100)
                                                    {
                                                        hasValues = true;
                                                        iContactCtr = iContactCtr + 100;
                                                    }
                                                    else
                                                    {
                                                        hasValues = false;
                                                        iContactCtr = iContactCtr + outputContactQuery.ListOfContact.Contact.Length;
                                                    }

                                                    LogFile.Out(iContactCtr + " contact records will be extracted.");

                                                    try
                                                    {
                                                        for (Int32 idxContact = 0; idxContact < outputContactQuery.ListOfContact.Contact.Length; idxContact++)
                                                        {
                                                            String pContactRowId = outputContactQuery.ListOfContact.Contact[idxContact].Id;
                                                            String pCreatedBy = outputContactQuery.ListOfContact.Contact[idxContact].CreatedByFullName;
                                                            DateTime pCreatedDate = outputContactQuery.ListOfContact.Contact[idxContact].CreatedDate;
                                                            String pModifiedBy = outputContactQuery.ListOfContact.Contact[idxContact].ModifiedBy.Substring(0, outputContactQuery.ListOfContact.Contact[idxContact].ModifiedBy.IndexOf(','));
                                                            DateTime pModifiedDate = outputContactQuery.ListOfContact.Contact[idxContact].ModifiedDate;

                                                            /*20170331 - start of additional fields*/
                                                            String pAccountId = outputContactQuery.ListOfContact.Contact[idxContact].AccountId;
                                                            String pAccountName = outputContactQuery.ListOfContact.Contact[idxContact].AccountName;
                                                            String pContactType = outputContactQuery.ListOfContact.Contact[idxContact].ContactType;
                                                            String pDivisionId = outputContactQuery.ListOfContact.Contact[idxContact].DivisionId;
                                                            String pDivisionName = outputContactQuery.ListOfContact.Contact[idxContact].DivisionName;
                                                            String pContactCountry = outputContactQuery.ListOfContact.Contact[idxContact].AlternateCountry;
                                                            String pGroupMember = outputContactQuery.ListOfContact.Contact[idxContact].IndexedPick0;
                                                            /*20170331 - end of additional fields*/

                                                            /*20170403 - start of additional field*/
                                                            String pOwnerId = outputContactQuery.ListOfContact.Contact[idxContact].OwnerId;
                                                            String pOwnerFullName = outputContactQuery.ListOfContact.Contact[idxContact].OwnerFullName;
                                                            /*20170403 - End of additional field*/

                                                            if (outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book != null)
                                                            {
                                                                if (outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book.Length > 0)
                                                                {
                                                                    LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ". Has " + outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book.Length + " associated books.");

                                                                    for (Int32 idxContactBk = 0; idxContactBk < outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book.Length; idxContactBk++)
                                                                    {
                                                                        String pListOfBookName = outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book[idxContactBk].BookName;

                                                                        LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ": " + pListOfBookName);
                                                                        result = LogFile.saveExtractedContactDetails("Contact", pContactRowId, pListOfBookName, pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountId, pAccountName, pContactType, pDivisionId, pDivisionName, pContactCountry, pGroupMember, pOwnerId, pOwnerFullName);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ". Has No Book Association.");
                                                                    result = LogFile.saveExtractedContactDetails("Contact", pContactRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountId, pAccountName, pContactType, pDivisionId, pDivisionName, pContactCountry, pGroupMember, pOwnerId, pOwnerFullName);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ". Has No Book Association.");
                                                                result = LogFile.saveExtractedContactDetails("Contact", pContactRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountId, pAccountName, pContactType, pDivisionId, pDivisionName, pContactCountry, pGroupMember, pOwnerId, pOwnerFullName);
                                                            }
                                                        }
                                                    }
                                                    catch (Exception ex1)
                                                    {
                                                        LogFile.Out("Error on Extracting Contact Details: " + ex1.Message);
                                                        hasValues = false;
                                                        result = "Error";
                                                    }
                                                }
                                                else
                                                {
                                                    hasValues = false;
                                                    result = "";
                                                }
                                            }
                                            else
                                            {
                                                hasValues = false;
                                                result = "";
                                            }
                                        }
                                        else
                                        {
                                            hasValues = false;
                                            result = "";
                                        }
                                    }
                                    catch (Exception ex2)
                                    {
                                        LogFile.Out("Error encountered on getContactDetails(): " + ex2.Message);
                                        LogFile.Out("Retry accessing to CRM after error");

                                        pSession.Destroy();

                                        pSession = EnterLoginCredentials();

                                        svcContact.Url = pSession.GetURL();
                                        svcContact.CookieContainer = pSession.GetCookieContainer();
                                        svcContact.RequestEncoding = Encoding.UTF8;
                                        svcContact.Timeout = 1200000;

                                        outputContactQuery = svcContact.ContactQueryPage(inputContactQuery);

                                        if (outputContactQuery != null)
                                        {
                                            if (outputContactQuery.ListOfContact.Contact != null)
                                            {
                                                if (outputContactQuery.ListOfContact.Contact.Length > 0)
                                                {
                                                    if (outputContactQuery.ListOfContact.Contact.Length >= 100)
                                                    {
                                                        hasValues = true;
                                                        iContactCtr = iContactCtr + 100;
                                                    }
                                                    else
                                                    {
                                                        hasValues = false;
                                                        iContactCtr = iContactCtr + outputContactQuery.ListOfContact.Contact.Length;
                                                    }

                                                    LogFile.Out(iContactCtr + " contact records will be extracted.");

                                                    try
                                                    {
                                                        for (Int32 idxContact = 0; idxContact < outputContactQuery.ListOfContact.Contact.Length; idxContact++)
                                                        {
                                                            String pContactRowId = outputContactQuery.ListOfContact.Contact[idxContact].Id;
                                                            String pCreatedBy = outputContactQuery.ListOfContact.Contact[idxContact].CreatedByFullName;
                                                            DateTime pCreatedDate = outputContactQuery.ListOfContact.Contact[idxContact].CreatedDate;
                                                            String pModifiedBy = outputContactQuery.ListOfContact.Contact[idxContact].ModifiedBy.Substring(0, outputContactQuery.ListOfContact.Contact[idxContact].ModifiedBy.IndexOf(','));
                                                            DateTime pModifiedDate = outputContactQuery.ListOfContact.Contact[idxContact].ModifiedDate;

                                                            /*20170331 - start of additional fields*/
                                                            String pAccountId = outputContactQuery.ListOfContact.Contact[idxContact].AccountId;
                                                            String pAccountName = outputContactQuery.ListOfContact.Contact[idxContact].AccountName;
                                                            String pContactType = outputContactQuery.ListOfContact.Contact[idxContact].ContactType;
                                                            String pDivisionId = outputContactQuery.ListOfContact.Contact[idxContact].DivisionId;
                                                            String pDivisionName = outputContactQuery.ListOfContact.Contact[idxContact].DivisionName;
                                                            String pContactCountry = outputContactQuery.ListOfContact.Contact[idxContact].AlternateCountry;
                                                            String pGroupMember = outputContactQuery.ListOfContact.Contact[idxContact].IndexedPick0;
                                                            /*20170331 - end of additional fields*/

                                                            /*20170403 - start of additional field*/
                                                            String pOwnerId = outputContactQuery.ListOfContact.Contact[idxContact].OwnerId;
                                                            String pOwnerFullName = outputContactQuery.ListOfContact.Contact[idxContact].OwnerFullName;
                                                            /*20170403 - End of additional field*/

                                                            if (outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book != null)
                                                            {
                                                                if (outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book.Length > 0)
                                                                {
                                                                    LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ". Has " + outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book.Length + " associated books.");

                                                                    for (Int32 idxContactBk = 0; idxContactBk < outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book.Length; idxContactBk++)
                                                                    {
                                                                        String pListOfBookName = outputContactQuery.ListOfContact.Contact[idxContact].ListOfBook.Book[idxContactBk].BookName;

                                                                        LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ": " + pListOfBookName);
                                                                        result = LogFile.saveExtractedContactDetails("Contact", pContactRowId, pListOfBookName, pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountId, pAccountName, pContactType, pDivisionId, pDivisionName, pContactCountry, pGroupMember, pOwnerId, pOwnerFullName);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ". Has No Book Association.");
                                                                    result = LogFile.saveExtractedContactDetails("Contact", pContactRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountId, pAccountName, pContactType, pDivisionId, pDivisionName, pContactCountry, pGroupMember, pOwnerId, pOwnerFullName);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                LogFile.Out("Saving Associated Books for Contact Row Id " + pContactRowId + ". Has No Book Association.");
                                                                result = LogFile.saveExtractedContactDetails("Contact", pContactRowId, "No Book", pBookName, pCreatedDateFrom, pCreatedDateTo, pCreatedBy, pCreatedDate, pModifiedBy, pModifiedDate, pAccountId, pAccountName, pContactType, pDivisionId, pDivisionName, pContactCountry, pGroupMember, pOwnerId, pOwnerFullName);
                                                            }
                                                        }
                                                    }
                                                    catch (Exception ex3)
                                                    {
                                                        LogFile.Out("Error on Extracting Contact Details: " + ex3.Message);
                                                        hasValues = false;
                                                        result = "Error";
                                                    }
                                                }
                                                else
                                                {
                                                    hasValues = false;
                                                    result = "";
                                                }
                                            }
                                            else
                                            {
                                                hasValues = false;
                                                result = "";
                                            }
                                        }
                                        else
                                        {
                                            hasValues = false;
                                            result = "";
                                        }
                                    }

                                }

                            }
                            catch(Exception exInnerCatch21)
                            {
                                LogFile.Out("Error on getContactDetails(): " + exInnerCatch21.Message);
                                hasValues = false;
                                result = "Error";
                            }
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                LogFile.Out("Error on getContactDetails(): " + ex.Message);
                result = "Error";
            }

            return result;

        }

        public static OnDemandSession EnterLoginCredentials()
        {
            LogFile.LogToFile("Initializing login credentials...");

            try
            {
                username = ConfigurationManager.AppSettings["username"].ToString();
                password = ConfigurationManager.AppSettings["password"].ToString();
                servername = ConfigurationManager.AppSettings["servername"].ToString();

                mSession = connectToCRMonDemand(servername, username, password);
            }
            catch (Exception ex)
            {
                LogFile.LogToFile("Error on Login Credentials --> " + ex.Message.ToString() + ". Retry login");

                try
                {
                    username = ConfigurationManager.AppSettings["username"].ToString();
                    password = ConfigurationManager.AppSettings["password"].ToString();

                    LogFile.LogToFile("Enter Username: " + username.ToString());
                    servername = ConfigurationManager.AppSettings["servername"].ToString();
                    LogFile.LogToFile("Enter Server Name: " + servername.ToString());

                    mSession = connectToCRMonDemand(servername, username, password);
                }
                catch (Exception ex1)
                {
                    LogFile.LogToFile("Error on Login Credentials --> " + ex1.Message.ToString());
                    mSession = null;
                }
            }

            return mSession;
        }

        public static OnDemandSession connectToCRMonDemand(String pServerName, String pUsername, String pPassword)
        {
            mSession = new OnDemandSession();

            try
            {
                //Retrieve Connecton Parameters
                mSession.server = pServerName;
                mSession.username = pUsername;
                mSession.password = pPassword;

                mSession.Establish();
            }
            catch (Exception ex)
            {
                LogFile.LogToFile("Error: connectToCRMonDemand --> " + ex.Message + ". Retry login");

                try
                {
                    //Retrieve Connecton Parameters
                    mSession.server = pServerName;
                    mSession.username = pUsername;
                    mSession.password = pPassword;

                    mSession.Establish();
                }
                catch (Exception ex1)
                {
                    LogFile.LogToFile("Error: connectToCRMonDemand --> " + ex1.Message);
                    mSession = null;
                }
            }

            return mSession;
        }
    }
}
