﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Configuration;
using System.IO;
using System.Data;
using System.Collections;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace CRM_Data_Extraction_Processor
{
    public class LogFile
    {
        public static String extractedDetailsFilename = "";
        public static String logFilename = "";
        public static String filePath = "";
        public static void Out(String strLogText)
        {
            Console.WriteLine(strLogText);
            LogToFile(strLogText);
        }

        /*
         * Changes Made:
         * 1. 20170301 - Add Created By, Created Date, Modified By, Modified Date fields on the output file
         * 2. 20170331 - Add Group Member, Account Type, WR Terr, AC Terr Division fields on the queried fields
         * 3. 20170331 - Change the text delimeter to "|"
         * 4. 20170403 - Replace text delimeter "|" with "\t", Added Owner field on the output file
         */
        public static String saveExtractedAccountDetails(String pRecordType, String pRowId, String pBookName, String pQueriedBookName, DateTime pCreatedDateFrom, DateTime pCreatedDateTo, String pRecordCreatedBy, DateTime pRecordCreatedDate, String pRecordModifiedBy, DateTime pRecordModifiedDate, String pAccountType, String pWRTerr, String pACTerr, String pDivisionName, String pDivisionId, String pGroupMember, String pMainPhone, String pBillToCountry, String pShipToCountry, String pOwnerId, String pOwnerName)
        {
            filePath = ConfigurationManager.AppSettings["ExcelFilesFolder"].ToString();

            if (extractedDetailsFilename == "")
            {
                Random random = new Random((int)DateTime.Now.Ticks);
                extractedDetailsFilename = pRecordType + "_BookName_" + pQueriedBookName + "_DateRange_" + pCreatedDateFrom.ToString("yyyyMMdd") + "-" + pCreatedDateTo.ToString("yyyyMMdd") +"_"  + random.Next().ToString() + ".txt";
            }

            String extractedFilesPath = filePath + pRecordType + "\\";
            bool isPathExist = Directory.Exists(extractedFilesPath);
            StreamWriter sw = null;
            try
            {
                if (!isPathExist)
                {
                    Directory.CreateDirectory(extractedFilesPath);
                }

                if (!File.Exists(extractedFilesPath + extractedDetailsFilename))
                {
                    sw = new StreamWriter(extractedFilesPath + extractedDetailsFilename);

                    sw.WriteLine(pRecordType + "Id\tBookName\tAccount Type\tWR Terr\tAC Terr\tDivision Id\tDivision Name\tGroup Member\tMain Phone\tBillToCountry\tShipToCountry\tOwnerId\tOwnerName\tCreated By\tCreated Date\tModified By\tModified Date");
                }
                else
                {
                    sw = File.AppendText(extractedFilesPath + extractedDetailsFilename);
                }

                try
                {
                    sw.WriteLine(pRowId + "\t" + pBookName + "\t" + pAccountType + "\t" + pWRTerr + "\t" + pACTerr + "\t" + pDivisionId + "\t" + pDivisionName + "\t" + pGroupMember + "\t" + pMainPhone + "\t" + pBillToCountry + "\t" + pShipToCountry + "\t" + pOwnerId + "\t" + pOwnerName + "\t" + pRecordCreatedBy + "\t" + pRecordCreatedDate + "\t" + pRecordModifiedBy + "\t" + pRecordModifiedDate);
                }
                catch (Exception ex)
                {
                    LogFile.Out("Error on saveExtractedAccountDetails: " + ex.Message.ToString());
                }
                finally
                {
                    sw.Dispose();
                    sw.Close();
                }
                
                return (extractedFilesPath + extractedDetailsFilename).Replace(@"\\", @"\").ToString();

            }
            catch (Exception ex)
            {
                LogFile.Out("Error on saveExtractedAccountDetails: " + ex.Message.ToString());
                return null;
            }
        }

        /*
         * Changes
         * 1. 20170331 - Cloned the copy of saveExtractedAccountDetails
         * 2. 20170331 - Change the text delimeter to "|"
         * 3. 20170403 - Replace text delimeter "|" with "\t", Added Owner field on the output file
         */

        public static String saveExtractedContactDetails(String pRecordType, String pRowId, String pBookName, String pQueriedBookName, DateTime pCreatedDateFrom, DateTime pCreatedDateTo, String pRecordCreatedBy, DateTime pRecordCreatedDate, String pRecordModifiedBy, DateTime pRecordModifiedDate, String pAccountId, String pAccountName, String pContactType, String pDivisionId, String pDivisionName, String pContactCountry, String pGroupMember, String pOwnerId, String pOwnerName)
        {
            filePath = ConfigurationManager.AppSettings["ExcelFilesFolder"].ToString();

            if (extractedDetailsFilename == "")
            {
                Random random = new Random((int)DateTime.Now.Ticks);
                extractedDetailsFilename = pRecordType + "_BookName_" + pQueriedBookName + "_DateRange_" + pCreatedDateFrom.ToString("yyyyMMdd") + "-" + pCreatedDateTo.ToString("yyyyMMdd") + "_" + random.Next().ToString() + ".txt";
            }

            String extractedFilesPath = filePath + pRecordType + "\\";
            bool isPathExist = Directory.Exists(extractedFilesPath);
            StreamWriter sw = null;
            try
            {
                if (!isPathExist)
                {
                    Directory.CreateDirectory(extractedFilesPath);
                }


                if (!File.Exists(extractedFilesPath + extractedDetailsFilename))
                {
                    sw = new StreamWriter(extractedFilesPath + extractedDetailsFilename);

                    sw.WriteLine(pRecordType + "Id\tBookName\tContact Type\tAccount Id\tAccount Name\tContact Country\tDivision Id\tDivision Name\tGroup Member\tOwnerId\tOwnerName\tCreated By\tCreated Date\tModified By\tModified Date");
                }
                else
                {
                    sw = File.AppendText(extractedFilesPath + extractedDetailsFilename);
                }

                try
                {
                    sw.WriteLine(pRowId + "\t" + pBookName + "\t" + pContactType + "\t" + pAccountId + "\t" + pAccountName + "\t" + pContactCountry + "\t" + pDivisionId + "\t" + pDivisionName + "\t" + pGroupMember  + "\t" + pOwnerId + "\t" + pOwnerName + "\t" + pRecordCreatedBy + "\t" + pRecordCreatedDate + "\t" + pRecordModifiedBy + "\t" + pRecordModifiedDate);
                }
                catch (Exception ex)
                {
                    LogFile.Out("Error on saveExtractedContactDetails: " + ex.Message.ToString());
                }
                finally
                {
                    sw.Dispose();
                    sw.Close();
                }
                

                return (extractedFilesPath + extractedDetailsFilename).Replace(@"\\", @"\").ToString();

            }
            catch (Exception ex)
            {
                LogFile.Out("Error on saveExtractedContactDetails: " + ex.Message.ToString());
                return null;
            }
        }

        public static void LogToFile(String strLogText)
        {
            if (logFilename == "")
            {
                Random random = new Random((int)DateTime.Now.Ticks);
                logFilename = System.IO.Path.GetFileName(System.Diagnostics.Process.GetCurrentProcess().ProcessName) + "_" + DateTime.Today.ToString("yyyyMMdd") +"_" + random.Next().ToString() + ".txt";
            }

            String path = ConfigurationManager.AppSettings["LogFilesFolder"].ToString();
            bool isPathExist = Directory.Exists(path);
            StreamWriter swLogTransaction;

            try
            {
                if (!isPathExist)
                {
                    Directory.CreateDirectory(path);
                }

                if (!File.Exists(path + logFilename))
                {
                    swLogTransaction = new StreamWriter(path + logFilename);
                }
                else
                {
                    swLogTransaction = File.AppendText(path + logFilename);
                }

                try
                {
                    swLogTransaction.WriteLine(DateTime.Now + " -- " + strLogText);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                }
                finally
                {
                    swLogTransaction.Dispose();
                    swLogTransaction.Close();
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

        

    }
}
